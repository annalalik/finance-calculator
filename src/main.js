import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Kalkulator from './components/Kalkulator.vue'
import Lokata from './components/Lokata.vue'
import WordpressPosts from './components/WordpressPosts.vue'
import WordpressPost from './components/WordpressPost.vue'
import ToDo from './components/ToDo.vue'
import ToDoV2 from './components/ToDoV2.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import './plugins/element.js'

library.add(faTrash)

Vue.use(VueRouter)

Vue.config.productionTip = false

Vue.component('font-awesome-icon', FontAwesomeIcon)


const routes = [
  { path: '/kalkulator', component: Kalkulator} ,
  { path: '/lokata', component: Lokata},
  { path: '/wordpressposts', component: WordpressPosts},
  { path: '/wordpressposts/:id', component: WordpressPost},
  { path: '/todo', component: ToDo},
  { path: '/todov2', component: ToDoV2}
]

const router = new VueRouter({
  mode: 'history',
  routes
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')     
