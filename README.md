# vue-calc

## Project description

```
Learning Vue.js. A few simple and short applications to learn about Vue.js framework.
```
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Extra info
Open via console using:
```
npm run serve
```
